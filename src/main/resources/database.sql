
drop database eletronics_store;
create database eletronics_store;
use eletronics_store;

-- REGIONS
create table regions (
	id int primary key auto_increment,
    name varchar(20) not null
);

insert into regions (name) values ('North');
insert into regions (name) values ('Northest');
insert into regions (name) values ('Center');
insert into regions (name) values ('South');
insert into regions (name) values ('Southest');


-- TYPES CONTACTS
create table types_contacts(
	id int primary key auto_increment,
    name varchar(20) not null
);

insert into types_contacts (name) values ('Cellphone');
insert into types_contacts (name) values ('Residencial');
insert into types_contacts (name) values ('Email');

-- PRODUCT
create table products (
	id int primary key auto_increment,
	code varchar(12) not null,
    name varchar(30) not null,
    created_at datetime default now(),
    updated_at datetime default null,
    deleted_at datetime default null
);

-- PRODUCT PRICE
create table products_price (
	id int primary key auto_increment,
    price double default 0,
    
    region_id int not null,
    foreign key(region_id) references region(id),
    
    product_id int not null,
    foreign key(product_id) references product(id),
    
    created_at datetime default now(),
    updated_at datetime default null,
    deleted_at datetime default null
);

-- CLIENT
create table clients (
	code varchar(8) not null,
    name varchar(30) not null,    
    created_at datetime default now(),
    updated_at datetime default null,
    deleted_at datetime default null
    
    -- address_id int not null,
    -- foreign key (address_id) references addresses(id)
);

-- STATES
create table states (
	id int primary key auto_increment,
    uf varchar(2) not null,
    name varchar(20) not null
);

insert into states values (null, 'AC', 'Acre');
insert into states values (null, 'AL', 'Alagoas');
insert into states values (null, 'AP', 'Amapá');
insert into states values (null, 'AM', 'Amazonas');
insert into states values (null, 'BA', 'Bahia');
insert into states values (null, 'DF', 'Distrito Federal');
insert into states values (null, 'ES', 'Espiríto Santo');
insert into states values (null, 'GO', 'Goiás');
insert into states values (null, 'MA', 'Maranhão');
insert into states values (null, 'MT', 'Mato Grosso');
insert into states values (null, 'MS', 'Mato Grosso do Sul');
insert into states values (null, 'MG', 'Minas Gerais');
insert into states values (null, 'PA', 'Pará');
insert into states values (null, 'PB', 'Paraíba');
insert into states values (null, 'PR', 'Paraná');
insert into states values (null, 'PE', 'Pernambuco');
insert into states values (null, 'PI', 'Piauí');
insert into states values (null, 'RJ', 'Rio de Janeiro');
insert into states values (null, 'RN', 'Rio Grande do Norte');
insert into states values (null, 'RS', 'Rio Grande do Sul');
insert into states values (null, 'RO', 'Rondônia');
insert into states values (null, 'RR', 'Roraima');
insert into states values (null, 'SC', 'Santa Catarina');
insert into states values (null, 'SP', 'São Paulo');
insert into states values (null, 'SE', 'Sergipe');
insert into states values (null, 'TO', 'Tocantins');

-- ADDRESS
create table addresses (
	id int primary key auto_increment,
    street varchar(100) not null,
    complement varchar(10),
    number smallint not null,
    city varchar(20) not null,
    
    state_id int not null,
    foreign key(state_id) references states(id),
    
    district varchar(20) not null,
    zipcode varchar(8) not null,
    
    client_code varchar(8) not null,
	foreign key(client_code) references clients(code)
);

-- CONTACT
create table contacts (
	id int primary key auto_increment,
    description varchar(100) not null,
    
    type_id int not null,
    foreign key(type_id) references type(id),
    
    client_code varchar(8) not null,
    foreign key(client_code) references clients(code)
);

-- STATUS ORDERS
create table status(
	id int primary key auto_increment,
    name varchar(20) not null
);

insert into status (name) values ('Not finished');
insert into status (name) values ('Finished');

-- ORDERS
create table orders (
	id bigint primary key auto_increment,
    
    status_id int not null,
    foreign key(status_id) references status(id),
    
    client_code varchar(8) not null,
    foreign key (client_code) references client(code),
    
    created_at datetime default now(),
    updated_at datetime default null,
    deleted_at datetime default null
);


create table orders_products (
	id bigint primary key auto_increment,
    
    product_id int not null,
    foreign key  (product_id) references products(id),
    
    order_id bigint not null,
    foreign key  (order_id) references orders(id),
    
    quantity int default 0,
    
    price double
);
