var order = null;

$(function() {
	loadOrder(function() {
		setClient(order.client);
		setTotal(order.total);
		setStatus(order.status);
		console.log(order);
		createRowsTableProducts(order.orderProducts);
		$('.order_id').text(order.id);
		$('.loading').addClass('hide');
		$('.see').removeClass('hide');
	});
});

function setTotal(total) {
	$('.total').text(total.toFixed(2));
}

function setStatus(status) {
	$('.status').text(status.name);
	
	if (status.id == 2) {
		$('.btn-edit').remove();
	} else {
		$('.btn-edit').attr('href', '/orders/edit/'+order.id);
	}
}

function loadOrder(callback) {
	api.orders.read(getCode()).done(function(data) {
		order = data;
		callback();
	});
}

function createRowsTableProducts(productsOrder) {
	var html = '<tr><td>{0}</td><td>{1}</td><td>{2}</td><td>{3}</td><td>{4}</td></tr>';

	var allHtml = $('<div></div>');
	productsOrder.forEach(function(productOrder) {
		allHtml.append(html.format(productOrder.product.code, productOrder.product.name, productOrder.quantity, 'US$ ' +productOrder.price.toFixed(2), 'US$ ' +(productOrder.price * productOrder.quantity).toFixed(2)));
	});
	
	$('.product-order-list').html(allHtml.html());
}

function setClient(client) {
	$('.client-name h5').text(client.name);
	
	var clientInfo = $('.client-info');
	clientInfo.find('.zipcode').text(client.address.zipcode);
	clientInfo.find('.street').text(client.address.street);
	clientInfo.find('.district').text(client.address.district);
	clientInfo.find('.number').text(client.address.number);
	clientInfo.find('.complement').text(client.address.complement);
	clientInfo.find('.city').text(client.address.city);
	clientInfo.find('.state').text(client.address.state.name);
	
	if (client.contacts.length == 0) {
		$('.list-contacts').html('<div class="full-width">This client has no contacts.</div>');
	} else {
		var htmlContact = '<div class="full-width"><span>{0} ({1})</span></div>';
		var htmlAllContacts = $('<div></div>');
		//
		client.contacts.forEach(function(contact) {
			htmlAllContacts.append(htmlContact.format(contact.description, contact.type.name));
		});
		
		$('.list-contacts').html(htmlAllContacts.html());
	}
}