var codeClient;
var lastModal;
var datatable;

$(function() {
	datatable = $('#table-products').DataTable( {
		 data: []
	});
	
	loadList();
	
	$('.btn-confirm-status').click(confirmStatus);
	$('.btn-close-modal').click(closeModal);
});

function loadList() {
	api.products.read().done(function(data) {
		data = data.map(function(obj) {
			return [
			        obj.code, 
			        obj.name, 
			        obj.deletedAt == null ? 'Active' : 'Inactive',
			        createButtons(obj)
			];
		});
		
		console.log(data);
		
		datatable.clear();
	    datatable.rows.add(data);
	    datatable.draw();
		
		createLinks();
	});
}

function createButtons(product) {
	var btSee 	   = '<a class="btn btn-default btn-xs" href="/products/' + product.code + '" role="button"><i class="fa fa-eye" aria-hidden="true"></i></a>';
	var btEdit 	   = '<a class="btn btn-default btn-xs" href="/products/edit/'+product.code+'" role="button"><i class="fa fa-pencil" aria-hidden="true"></i></a>';
	var btInactive = '<a class="btn btn-modal btn-default btn-xs" href="#" data-toggle="modal" data-code="'+product.code+'" data-name="'+product.name+'" data-target="#inactive-modal" role="button"><i class="fa fa-ban" aria-hidden="true"></i></a>';
	var btActive   = '<a class="btn btn-modal btn-default btn-xs" href="#" data-toggle="modal" data-code="'+product.code+'" data-name="'+product.name+'" data-target="#active-modal" role="button"><i class="fa fa-ban" aria-hidden="true"></i></a>';
	
	var buttons = btSee+btEdit;
	buttons += product.deletedAt == null ? btInactive : btActive;
	return buttons;
}

function createLinks() {
	$('.btn-modal').on('click', function(data){
		codeClient = $(this).data('code');
		lastModal = $($(this).data('target'));
		lastModal.find('.content').removeClass('hide');
		lastModal.find('.load').addClass('hide');
		lastModal.find('.btns').removeClass('hide');
		lastModal.find('.btn-close-modal').addClass('hide');
		
		var name = $(this).data('name');
		$('.product-name').text(name);
		
		lastModal.modal({
			show: true
		});
	});
}

function confirmStatus() {
	var type = $(this).data('type');
	lastModal.find('.content').addClass('hide');
	lastModal.find('.load').removeClass('hide');
	lastModal.find('.btns').addClass('hide');
	api.products[type](codeClient).done(function(data){
		lastModal.find('.btn-close-modal').removeClass('hide');
		lastModal.find('.load').addClass('hide');
		lastModal.find('.status').removeClass('hide');
		lastModal.find('.close').removeClass('hide');
	});
}

function closeModal() {
	lastModal.modal({
		show: false
	});
	
	loadList();
}