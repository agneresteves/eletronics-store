var client;
$(function() {
	loadClient();
});

function loadClient() {
	var code = getCode();
	api.clients.read(code).done(function(data) {
		client = data;
		setInfo(client);
	});
}

function setInfo(client) {	
	$('span[data-field]').each(function(k, field){
		var name = $(field).data('field');
		var parts = name.split(".");
		
		var value = client;
		
		var i = 0;
		do {
			value = value[parts[i]];
			i++;
		} while(i < parts.length);
		
		$(this).text(value);
	});
	
	if (client.contacts.length == 0) {
		$('.no-contacts').removeClass('hide');
	} else {
		var contactHtml = '<div class="form-row default"><div class="form-group col-md-6"><label class="col-form-label">{0}</label></div><div class="form-group col-md-6"><label class="col-form-label">{1}</label></div></div>';
		
		var allContactsHtml = $('<div></div>');
		client.contacts.forEach(function(contact) {
			allContactsHtml.append(contactHtml.format(contact.type.name, contact.description));
		});
		$('.contact-list').append(allContactsHtml.html()).removeClass('hide');
	}

	$('.btn-edit').attr('href', '/clients/edit/'+client.code);
	
	
	$('.loading').addClass('hide');
	$('.see').removeClass('hide');
}