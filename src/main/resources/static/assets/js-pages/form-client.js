var modelContactInput;
var modalForm;
var typeAction;
var allContacts;
var allStates;
var formClient;

$(function() {
	modalForm = $('#send-form-modal');
	typeAction = $('.form').hasClass('edit') ? 'edit' : 'new';
	
	createForm();
	
	if (typeAction == 'edit') {
		api.states.read().done(function(data) {
			allStates = data;
			loadClient();
		});
	} else {
		api.states.read().done(function(data) {
			allStates = data;
			api.typescontacts.read().done(function(data) {
				allContacts = data;
				createContactRow();
				createSelectStates();
				startAddContact();
			});
		});
	}
});

function createForm() {
	formClient = new FormWizard('.form-client', {
		code: {
			
		},
        id: {

        },
        name: {
            rules: 'required|min:3|max:100'
        },
        street: {
            rules: 'required'
        },
        zipcode: {
            rules: 'required',
            mask: '00000-000',
            onCompleteMask: function(cep) {
                searchCep(cep);
            },
            hasLoading: true
        },
        number: {
            rules: 'required'
        },
        complement: {
            
        },
        district: {
            rules: 'required'
        },
        state: {
            rules: 'required',
            select: true
        },
        city: {
            rules: 'required|min:3'
        },
        typeContacts: {
        	select: true
        },
        descriptionContacts: {
        	
        }
    }, function() {
    	submit();
    });
}

function submit() {
    sendForm(createValues());
}

function createValues() {
	var values = {
		code: formClient.get('code'),
		name: formClient.get('name'),
		address: {
			id: Number(formClient.get('id')),
			street: formClient.get('street'),
			district: formClient.get('district'),
			number: Number(formClient.get('number')),
			complement: formClient.get('complement'),
			city: formClient.get('city'),
			state: {
				id: Number(formClient.get('state')),
			},
			zipcode: formClient.get('zipcode').replace('-', '')
		},
    	contacts: createContacts(formClient.get('typeContacts'), formClient.get('descriptionContacts'))
    };
	
    if (!values.code) {
    	delete values.code;
    }
    if (!values.address.id) {
    	delete values.address.id;
    }
    return values;
}

function createContacts(typeContacts, descriptionContacts) {
	
	if (Array.isArray(typeContacts)) {
		var objs = [];
		
		for (var i in typeContacts) {
			if (!descriptionContacts[i].trim()) {
				continue;
			}
			
			objs.push({
				description: descriptionContacts[i].trim(),
				type: {
					id: Number(typeContacts[i])
				}
			});
		}
		
		
		return objs;
	}
	
	return [{
		description: descriptionContacts,
		type: {
			id: Number(typeContacts)
		}
	}];
}

function maskCep(zipcode) {
    var div = $('<div>'+zipcode+'</div>').mask('00000-000');
    return div.text();
}

function searchCep(cep) {
    var fieldsAddress = ['zipcode', 'street', 'number', 'district', 'complement', 'city', 'state'];

    formClient.setDisabled(fieldsAddress);
    CEP.getInfo(cep, function(address) {
    	allStates.forEach(function(state) {
    		if (address.state == state.uf) {
    			address.state = state.id;
    		}
    	});
    	
    	formClient.setValue(address);
    	formClient.setEnabled(fieldsAddress);
    	formClient.validateAllFields();
    	formClient.verifySubmitEnabled();
    });
};

function loadClient() {
	api.typescontacts.read().done(function(data) {
		allContacts = data;
		
		createContactRow();
		createSelectStates();
		
		api.clients.read(getCode()).done(function(data) {
			setField(data);
		});
	});
}

function createSelectStates() {
	var opt = '<option value="{0}">{1}</option>';
	var htmlOptions = $('<div></div>');
	
	allStates.forEach(function(state) {
		htmlOptions.append(opt.format(state.id, state.name));
	});
	
	$('.select-state').html(htmlOptions.html());
}

function setField(client) {	
	var address = client.address;
	
	address.zipcode = maskCep(address.zipcode);
	
	formClient.setValue(client);
    formClient.setValue(address);
    formClient.setAllEnabled();
    formClient.validateAllFields();
    formClient.verifySubmitEnabled();
    
    createContactsRows(client.contacts);
    startAddContact();
    
	$('.btn-edit').attr('href', '/clients/edit/'+client.code);
	
	$('.loading').addClass('hide');
	$('.form').removeClass('hide');
}

function startAddContact() {
	modelContactInput = '<div class="form-row default">'+$('.contact-list .default').html()+'</div>';
    $('.btn-add-contact').click(function(){
		addContact();
	});
}

function mapContacts() {
	allContacts = allContacts.map(function(contact, k) {
		return {
			html: '<option SEL value="{0}">{1}</option>'.format(contact.id, contact.name),
			contact: contact
		}
	});
}

function createContactRow() {
	mapContacts();
	
	var allContactsHtml = $('<div></div>');
	var contactHtml =$('<div class="form-row default"><div class="form-group col-md-6"><label class="col-form-label">Type</label><select name="typeContacts[]" data-field="typeContacts" required="required" class="form-control typeContacts"></select></div><div class="form-group col-md-6"><label class="col-form-label">Description</label><input type="text" data-field="descriptionContacts" name="descriptionContacts[]" class="form-control" value="" placeholder="Enter description"></input></div></div>');
	var options = $('<div></div>');
	
	allContacts.forEach(function(option) {
		options.append(option.html.replace("SEL", ""));
	});

	contactHtml.find('.typeContacts').html(options.html());
	allContactsHtml.html(contactHtml);
	$('.contact-list').html(allContactsHtml.html());
	formClient.reload();
}

function createContactsRows(contacts) {
	var allContactsHtml = $('<div></div>');
	contacts.forEach(function(contact) {		
		var contactHtml =$('<div class="form-row"><div class="form-group col-md-6"><label class="col-form-label">Type</label><select name="typeContacts[]" data-field="typeContacts" required="required" class="form-control typeContacts"></select></div><div class="form-group col-md-6"><label class="col-form-label">Description</label><input type="text" data-field="descriptionContacts" name="descriptionContacts[]" class="form-control" value="'+contact.description+'" placeholder="Enter description"></input></div></div>');
		
		var options = $('<div></div>');
		
		allContacts.forEach(function(option) {
			options.append(option.html.replace("SEL", contact.type.id == option.contact.id ? 'selected="selected"' : ""));
		});

		contactHtml.find('.typeContacts').html(options.html());
		allContactsHtml.append(contactHtml);
	});
	
	$('.contact-list').prepend(allContactsHtml.html()).removeClass('hide');
	formClient.reload();
	formClient.validateAllFields();
}

function addContact() {
	$('.contact-list').append(modelContactInput);
	formClient.reload();
	formClient.validateAllFields();
}

function sendForm(client) {
	modalForm.modal({
		show: true,
		keyboard: false,
		backdrop: 'static'
	});
	
	$('.modal-footer').addClass('hide');
	$('.load').removeClass('hide');
	$('.status').addClass('hide');

	if (typeAction == 'new') {
		api.clients.create(client, {}).done(function(data) {
			completeForm(data);
		});
	} else {
		api.clients.update(getCode(), client).done(function(data) {
			completeForm(data);
		});
	}
}

function completeForm(data) {
	$('.client-name').text(data.name);
	$('.load').addClass('hide');
	$('.status').removeClass('hide');
	$('.modal-footer').removeClass('hide');
	$('.btn-close-modal').attr('href', '/clients/'+data.code);
}