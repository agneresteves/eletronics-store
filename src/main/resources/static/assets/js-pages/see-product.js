var htmlRegion;
var product;

$(function() {
	htmlRegion = '<div class="form-row default"><div class="form-group col-md-6"><label class="col-form-label">{0}</label></div><div class="form-group col-md-6"><label class="col-form-label">{1}</label></div></div>';
	
	api.products.read(getCode()).done(function(productApi) {
		product = productApi;
		setInfo();
	});
	
});

function setInfo() {
	$('[data-field="code"]').text(product.code);
	$('[data-field="name"]').text(product.name);
	
	createInputRegion(product.productPrices);
	showForm();
	$('.btn-edit').attr('href', '/products/edit/'+product.code);
}

function showForm() {
	$('.loading').addClass('hide');
	$('.form-product').removeClass('hide');
}

function createInputRegion(regions) {
	var htmlsRegions = $('<div></div>');
	regions.forEach(function(pp) {
		htmlsRegions.append(htmlRegion.format(pp.region.name, "US$ " + pp.price.toFixed(2), 0));
	});
	$('.products-price').html(htmlsRegions.html());
}