var modalForm;
var typeAction;
var allContacts;
var formProduct;
var htmlRegion;

$(function() {
	htmlRegion = '<div class="form-group col-md-6"><input name="regionsCode" type="hidden" value="{0}" data-field="regionId"></input><span class="full-width">{1}</span></div><div class="form-group col-md-6"><div class="input-group mb-2 mb-sm-0"><div class="input-group-addon">US$</div><input type="number" data-field="price" value="{2}" min="0" value="0" class="form-control" required="required" step="0.1" placeholder="Enter price for"></input></div></div>';
	
	modalForm = $('#send-form-modal');
	typeAction = $('.form-product').hasClass('edit') ? 'edit' : 'new';
	
	createForm();
	
	if (typeAction == 'edit') {
		loadProduct();
	} else {
		api.regions.read().done(function(regions) {
			createInputRegion(regions);
			showForm();
		});
	}
});

function loadProduct() {
	api.products.read(getCode()).done(function(data) {
		console.log(data);
		setField(data);
	});
}

function setField(product) {
	formProduct.setValue(product);
    formProduct.setAllEnabled();
    if (typeAction == 'edit') {
    	formProduct.setDisabled(['code']);
    }
    
    createInputRegion(product.productPrices);
    
    formProduct.validateAllFields();
    formProduct.verifySubmitEnabled();
    
    showForm();
}

function showForm() {
	$('.loading').addClass('hide');
	$('.form-product').removeClass('hide');
}

function createInputRegion(regions) {
	var htmlsRegions = $('<div></div>');
	regions.forEach(function(pp) {
		if (typeAction == 'new') {
			htmlsRegions.append(htmlRegion.format(pp.id, pp.name, 0));
		} else {
			htmlsRegions.append(htmlRegion.format(pp.region.id, pp.region.name, pp.price));
		}
		
	});
	$('.products-price').html(htmlsRegions.html());
	formProduct.reload();
}

function createForm() {
	formProduct = new FormWizard('.form-product', {
        id: {

        },
        code: {
        	rules: 'required|min:12|max:12'
		},
        name: {
            rules: 'required|min:3|max:100',
        },
        regionId: {
        	
        },
        price: {
        	rules: 'required'
        }
    }, function() {
    	submit();
    });
}

function submit() {
	var values = createValues();
	
	modalForm.modal({
		show: true
	});
	
	$('.modal-footer').addClass('hide');
	$('.load').removeClass('hide');
	$('.status').addClass('hide');
	
	if (typeAction == 'new') {
		api.products.create(values, {}).done(function(data) {
			completeForm(data);
		});
	} else {
		api.products.update(getCode(), values).done(function(data) {
			completeForm(data);
		});
	}
}

function completeForm(data) {
	$('.client-name').text(data.name);
	$('.load').addClass('hide');
	$('.status').removeClass('hide');
	$('.modal-footer').removeClass('hide');
	$('.btn-close-modal').attr('href', '/products/'+data.code);
}

function createValues() {
	var values = {
	     id: Number(formProduct.get('id')),
	     code: formProduct.get('code'),
	     name: formProduct.get('name'),
	     productPrices: createProductPrices()
	};
	
	return values;
}

function createProductPrices() {
	var regionsIds = formProduct.get('regionId');
	var prices     = formProduct.get('price')
	
	if (Array.isArray(regionsIds)) {
		var objs = [];
		
		for (var i in regionsIds) {
			objs.push({
				price: prices[i],
				region: {
					id: regionsIds[i]
				}
			});
		}
		
		return objs;
	}
	
	return [{
		price: prices,
		region: {
			id: regionsIds
		}
	}];
}
