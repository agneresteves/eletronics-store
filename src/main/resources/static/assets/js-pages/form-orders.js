var allClients = [];
var clientSelectedCode = null;
var clientSelected = {};
var order = null;
var allProducts = [];
var productsSelecteds = [];
var productSelectedCode = null;

var productsValues = [];

var tableProducts = null;

var formOrder = null;
var typeAction = null;

$(function() {
	typeAction = $('.form-order').hasClass('edit') ? 'edit' : 'new';
	
	createForm();
	
	if (typeAction == 'edit') {
		loadOrder(function() {
			if (order.status.id == 2) {
				$('.alert-impossible-edit').removeClass('hide');
				$('.loading').addClass('hide');
			} else {
				loadEntities();
			}
		});
	} else {
		loadEntities();
	}
	
	$('.product-order-list').on('keyup', 'input[data-field="quantity"]', function() {
		changeQuantityProduct($(this));
	});
	
	$('.product-order-list').on('keyup', 'input[data-field="price"]', function() {
		changePriceProduct($(this));
	});
	
	$('.btn-modal-product').click(function() {
		openModalProducts();
	});
	
	$('.form-order').on('click', '.remove-product', function(){
		removeProduct($(this));
	});
});

function removeProduct(el) {
	var tr = el.parent().parent();
	var code = tr.data('code');
	delete productsValues[code];
	changeTotal();
	tr.remove();
	
	if (productsValues.length == 0) {
		$('.product-order-list .no-products').removeClass('hide');
	}
}

function loadOrder(callback) {
	api.orders.read(getCode()).done(function(data) {
		order = data;
		callback();
	});
}

function loadEntities() {
	loadClients(function() {
		loadProductsAvailables(function() {
			createTableClients();
			createTableProducts();
			
			if (typeAction == 'edit') {
				setInfo();
			}
			
			showForm();
		});
	});
}

function setInfo() {
	$('.order_id').text(": #"+order.id);
	$('.total').text(order.total.toFixed(2));
	
	formOrder.setValue({status: order.status.id});
	
	setInfoClient(order.client);
	addProducts();
	formOrder.reload();
	formOrder.validateAllFields();
}

function addProducts() {
	$('.product-order-list .no-products').addClass('hide');
	
	order.orderProducts.forEach(function(orderProduct) {
		productsValues[orderProduct.product.code] = {
			quantity: orderProduct.quantity,
			price: orderProduct.price.toFixed(2),
			subtotal: (orderProduct.quantity * orderProduct.price).toFixed(2)
		};
		
		createHtmlProduct(orderProduct.product, orderProduct.id);
	});
}

function createForm() {
	formOrder = new FormWizard('.form-order', {
		id: {
			
		},
		client: {
			rules: 'required'
		},
		status: {
			rules: 'required',
			select: true
		},
		product: {
			rules: 'required|min:1'
		},
		product_order_id: {
			
		},
		quantity: {
			rules: 'required|minValue:1'
		},
		price: {
			rules: 'required|minValue:0.1'
		}
	}, function() {
		sendForm();
	});
}

function sendForm() {	
	var values = {
		client: {
			code: formOrder.get('client')
		},
		status: {
			id: Number(formOrder.get('status'))
		},
		orderProducts: createProductsToSend(formOrder.get('product_order_id'), formOrder.get('product'), formOrder.get('quantity'), formOrder.get('price'))
	};
	
	$('.zero-products-alert').addClass('hide');
	$('.load').removeClass('hide');
	var modalFooter = $('.footer-send-form');
	modalFooter.addClass('hide');
	modalFooter.find('.btn-cancel').addClass('hide');
	
	$('#send-form-modal').modal({
		backdrop: 'static',
		keyboard: false,
		show: true
	});
	
	if (values.orderProducts.length == 0) {
		showAlertNoProducts();
		return;
	}
	
	if (typeAction == 'edit') {
		values.id = getCode();
		api.orders.update(getCode(), values).done(function(order) {
			completeForm(order);
		});
	} else {		
		api.orders.create(values, {}).done(function(order) {
			completeForm(order);
		});
	}
}

function showAlertNoProducts() {
	$('.zero-products-alert').removeClass('hide');
	$('.load').addClass('hide');
	var modalFooter = $('.footer-send-form');
	modalFooter.removeClass('hide');
	modalFooter.find('.btn-cancel').removeClass('hide');
}

function completeForm(order) {
	$('.load').addClass('hide');
	$('.status').removeClass('hide');
	
	var modalFooter = $('.footer-send-form');
	modalFooter.removeClass('hide');
	modalFooter.find('.btn-ok').removeClass('hide').attr('href', '/orders/'+order.id);
}

function createProductsToSend(productOrderId, products, quantities, prices) {
	if (Array.isArray(products)) {
		var objs = [];
		
		for (var i in products) {
			var item = {
				product: {
					id: Numbeer(products[i])
				},
				price: Number(prices[i]),
				quantity: Number(quantities[i])
			};
			
			if (typeAction == 'edit') {
				item.id = Number(productOrderId[i]);
			}
			
			objs.push(item);
		}
		
		return objs;
	}
	
	var item = {
		product: {
			id: Number(products)
		},
		price: Number(prices),
		quantity: Number(quantities)
	};
	
	if (typeAction == 'edit') {
		item.id = Number(productOrderId);
	}
	
	return [item];
}

function changeQuantityProduct(field) {
	var code = field.parent().parent().parent().data('code');
	productsValues[code].quantity = Number(field.val());
	drawProductValues(code);
}

function changePriceProduct(field) {
	var code = field.parent().parent().parent().data('code');
	productsValues[code].price = Number(field.val());
	drawProductValues(code);
}

function drawProductValues(code) {
	var tr = $('.product-order-list tr[data-code="'+code+'"]');
	tr.find('input[data-field="quantity"]').attr('value', productsValues[code].quantity);
	productsValues[code].subtotal = productsValues[code].quantity * productsValues[code].price;
	tr.find('.subtotal span').text(productsValues[code].subtotal.toFixed(2));
	changeTotal();
}

function changeTotal() {
	var total = 0;

	for (var i in productsValues) {
		total += Number(productsValues[i].subtotal);
	}

	$('.total').text(total.toFixed(2));
}

function confirmSelectedClient() {	
	allClients.forEach(function(client) {
		if (client.code == clientSelectedCode) {
			clientSelected = client;
			setInfoClient(client);
			formOrder.validateAllFields();
		}
	});
}

function setInfoClient(client) {
	$('.client-name h5').text(client.name);
	
	var clientInfo = $('.client-info');
	clientInfo.find('.zipcode').text(client.address.zipcode);
	clientInfo.find('.street').text(client.address.street);
	clientInfo.find('.district').text(client.address.district);
	clientInfo.find('.number').text(client.address.number);
	clientInfo.find('.complement').text(client.address.complement);
	clientInfo.find('.city').text(client.address.city);
	clientInfo.find('.state').text(client.address.state.name);
	
	if (client.contacts.length == 0) {
		$('.list-contacts').html('<div class="full-width">This client has no contacts.</div>');
	} else {
		var htmlContact = '<div class="full-width"><span>{0} ({1})</span></div>';
		var htmlAllContacts = $('<div></div>');
		//
		client.contacts.forEach(function(contact) {
			htmlAllContacts.append(htmlContact.format(contact.description, contact.type.name));
		});
		
		$('.list-contacts').html(htmlAllContacts.html());
	}
	
	$('input[data-field="client"]').attr('value', client.code);
	$('.info-client').removeClass('hide');
}

function setSelectedClient(tr) {
	clientSelectedCode = tr.data('code');
	$('.table-clients tr.client').removeClass('selected');
	tr.addClass('selected');
}

function setSelectedProduct(tr) {
	productSelectedCode = tr.data('code');
	deselectAllProducts();
	tr.addClass('selected');
}
function deselectAllProducts() {
	$('.table-products tr.product').removeClass('selected');
}

function loadClients(callback) {
	api.clients.read().done(function(clients) {
		allClients = clients;
		callback();
	});
}

function loadProductsAvailables(callback) {
	api.products.read('available').done(function(products) {
		allProducts = products;
		callback();
	});
} 

function createTableClients() {
	var listClients = $('.table-body-clients');
	var html = '<tr class="client" data-code="{0}"><td>{1}</td><td>{2}</td></tr>';
	
	var allHtmls = $('<div></div>');

	allClients.forEach(function(client) {
		allHtmls.append(html.format(client.code, client.code, client.name));
	});
	
	listClients.html(allHtmls.html());
	
	$('.table-clients').DataTable();
}

function createTableProducts() {
	var listProducts = $('.table-body-products');
	var html = '<tr class="product" data-code="{0}"><td>{1}</td><td>{2}</td></tr>';
	
	var allHtmls = $('<div></div>');

	allProducts.forEach(function(product) {
		allHtmls.append(html.format(product.code, product.code, product.name));
	});
	
	listProducts.html(allHtmls.html());
	
	$('.table-products').DataTable();
}

function showForm() {
	$('.table-clients').on('click', 'tr.client', function() {
		setSelectedClient($(this));
	});
	
	$('.table-products').on('click', 'tr.product', function() {
		setSelectedProduct($(this));
	});
	
	$('.btn-confirm-client').click(function() {
		confirmSelectedClient();
	});
	
	$('.btn-add-product').click(function() {
		confirmSelectedProduct();
	});
	
	$('.loading').addClass('hide');
	$('.form-order').removeClass('hide');
}

function confirmSelectedProduct() {
	if (productsValues[productSelectedCode]) {
		productsValues[productSelectedCode].quantity++;
		drawProductValues(productSelectedCode);
		formOrder.validateAllFields();
		return;
	}
	
	$('.product-order-list .no-products').addClass('hide');
	
	var productSelected = {};
	allProducts.forEach(function(product) {
		if (productSelectedCode == product.code) {
			productSelected = product;
		}
	});
	
	productsValues[productSelected.code] = {
		quantity: 1,
		price: Number(0.1).toFixed(2),
		subtotal: (0.1).toFixed(2)
	};
	
	createHtmlProduct(productSelected);
	changeTotal();
	
	formOrder.reload();
	formOrder.validateAllFields();
}

function createHtmlProduct(product, orderProductId) {
	var htmlProduct = '<tr data-code="{0}"><td scope="row"><input type="hidden" data-field="product_order_id" value="{1}"><input type="hidden" data-field="product" value="{2}"><span class="id">{3}</span></td><td><span class="product-name">{4}</span></td><td><div class="form-group"><input class="form-control" value="{5}" data-field="quantity" type="number" min="0"></input></div></td><td><div class="form-group"><input class="form-control" value="{6}" data-field="price" type="number" step="0.1" min="0"></input></div></td><td><h6 class="subtotal">US$ <span>{7}</span></h6></td><td><button class="btn btn-light remove-product"><i class="fa fa-trash" aria-hidden="true"></i></button></td></tr>'.format(product.code, orderProductId ? orderProductId : 0, product.id, product.code, product.name, productsValues[product.code].quantity, productsValues[product.code].price, productsValues[product.code].price);
	$('.product-order-list').append(htmlProduct);
}

function openModalProducts() {
	deselectAllProducts();
	$('#add-product').modal({
		keyboard: false,
		backdrop: 'static',
		show: true
	});
}