var CEP = {

    getInfo: function(cep, callback) {
        $.getJSON('https://viacep.com.br/ws/'+cep+'/json/', function(info) {
            if (info.erro) {
                callback(info);
                return;
            }

            var address = {
                street: info.logradouro,
                city: info.localidade,
                district: info.bairro,
                state: info.uf
            };

            callback(address);
        });
    }

};