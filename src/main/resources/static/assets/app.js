var api;

$(document).ready(function() {
    //$('#table-home-dash').DataTable();
	$.ajaxSetup({ contentType: "application/json; charset=utf-8", });
	
    //activateValidationForm();
    
    api = new $.RestClient('/api/', {
    	stringifyData: true
    });
    
    api.add('clients');
    api.clients.addVerb('active', 'post');
    api.clients.addVerb('inactive', 'post');
    
    api.add('states');
    
    api.add('products');
    api.products.addVerb('active', 'post');
    api.products.addVerb('inactive', 'post');
    api.products.add('available');
    
    api.add('orders');
    api.orders.addVerb('active', 'post');
    api.orders.addVerb('inactive', 'post');
    
    api.add('contacts');
    api.add('typescontacts');
    api.add('regions');
    
    if (!String.prototype.format) {
        String.prototype.format = function() {
            var formatted = this;
            for (var i = 0; i < arguments.length; i++) {
                var regexp = new RegExp('\\{'+i+'\\}', 'gi');
                formatted = formatted.replace(regexp, arguments[i]);
            }
            return formatted;
        }
    };
    
});

function activateValidationForm() {
	var form = document.getElementById("needs-validation");
	if (form) {
		form.addEventListener("submit", function(event) {
	      if (form.checkValidity() == false) {
	        event.preventDefault();
	        event.stopPropagation();
	      }
	      form.classList.add("was-validated");
	    }, false);
	}
}

function getCode() {
	var pathname = location.pathname.split("/");
	return pathname[pathname.length-1];
}