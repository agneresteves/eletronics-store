package br.com.agneresteves.restcontroller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.agneresteves.domain.Order;
import br.com.agneresteves.service.OrderService;

@RestController
@RequestMapping("/api/orders")
public class OrderRestController {
  
	@Autowired
    private OrderService orderService;
   
    @RequestMapping(method = RequestMethod.GET)
    public Iterable<Order> findAll() {
    	return this.orderService.findAll();
    }
    
    @RequestMapping(method = RequestMethod.POST)
    public Order store(@RequestBody Order order) {
        return this.orderService.save(order);
    }
    
    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public Order update(@PathVariable long id, 
    					@RequestBody Order order) {
    	order.setId(id);
    	return this.orderService.update(order);
    }
    
    @RequestMapping(value = "/{id}/cancel", method = RequestMethod.POST)
    public Order cancel(@PathVariable long id) {
        Order order = this.orderService.findOne(id);
        return this.orderService.cancel(order);
    }

    @RequestMapping(value = "/{id}/restore", method = RequestMethod.POST)
    public Order restore(@PathVariable long id) {
        Order order = this.orderService.findOne(id);
        return this.orderService.restore(order);
    }
    
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Order findOne(@PathVariable long id) {
    	return this.orderService.findOne(id);
    }
    
}
