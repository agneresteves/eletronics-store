package br.com.agneresteves.restcontroller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.agneresteves.domain.Region;
import br.com.agneresteves.service.RegionService;

@RestController
@RequestMapping("/api/regions")
public class RegionRestController {
  
    @Autowired
    private RegionService regionService;
   
    @RequestMapping(method = RequestMethod.GET)
    public Iterable<Region> findAll() {
    	return this.regionService.findAll();
    }
    
}
