package br.com.agneresteves.restcontroller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.agneresteves.domain.TypeContact;
import br.com.agneresteves.service.TypeContactService;

@RestController
@RequestMapping("/api/typescontacts")
public class TypeContactRestController {
  
    @Autowired
    private TypeContactService typeContactService;
   
    @RequestMapping(method = RequestMethod.GET)
    public Iterable<TypeContact> findAll() {
    	return this.typeContactService.findAll();
    }
    
}
