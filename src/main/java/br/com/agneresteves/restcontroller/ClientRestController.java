package br.com.agneresteves.restcontroller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.agneresteves.domain.Client;
import br.com.agneresteves.service.ClientService;

@RestController
@RequestMapping("/api/clients")
public class ClientRestController {
  
    @Autowired
    private ClientService clientService;
   
    @RequestMapping(method = RequestMethod.GET)
    public Iterable<Client> findAll() {
    	return this.clientService.findAll();
    }
    
    @RequestMapping(value = "/{code}", method = RequestMethod.GET)
    public Client findByCode(@PathVariable String code) {
    	return this.clientService.findByCode(code);
    }
    
    @RequestMapping(method = RequestMethod.POST)
    public Client store(@RequestBody Client client) {
        return this.clientService.save(client);
    }
    
    @RequestMapping(value = "/{code}", method = RequestMethod.PUT)
    public Client update(@PathVariable String code, 
                         @RequestBody Client client) {
        return this.clientService.update(client);
    }
    
    @RequestMapping(value = "/{code}/inactive", method = RequestMethod.POST)
    public Client inactive(@PathVariable String code) {
        Client client = this.clientService.findByCode(code);
        return this.clientService.inactive(client);
    }

    @RequestMapping(value = "/{code}/active", method = RequestMethod.POST)
    public Client active(@PathVariable String code) {
        Client client = this.clientService.findByCode(code);
        return this.clientService.active(client);
    }
    
    
}
