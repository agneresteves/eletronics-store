package br.com.agneresteves.restcontroller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.agneresteves.domain.Product;
import br.com.agneresteves.service.ProductService;

@RestController
@RequestMapping("/api/products")
public class ProductRestController {

  @Autowired
  private ProductService productService;
 
  @RequestMapping(method = RequestMethod.GET)
  public Iterable<Product> findAll() {
      return this.productService.findAll();
  }
  
  @RequestMapping(method = RequestMethod.GET, value = "/{code}")
  public Product findByCode(@PathVariable String code) {
      return this.productService.findByCode(code);
  }
  
  @RequestMapping(method = RequestMethod.GET, value = "/available")
  public Iterable<Product> findAvailable() {
      return this.productService.findAvailable();
  }
  
  @RequestMapping(method = RequestMethod.POST)
  public Product store(@RequestBody Product product) {
      return this.productService.save(product);
  }
  
  @RequestMapping(value = "{code}", method = RequestMethod.PUT)
  public Product update(@RequestBody Product product) {
      return this.productService.save(product);
  }
  
  @RequestMapping(value = "/{code}/inactive", method = RequestMethod.POST)
  public Product inactive(@PathVariable String code) {
	    Product product = this.productService.findByCode(code);
      return this.productService.inactive(product);
  }

  @RequestMapping(value = "/{code}/active", method = RequestMethod.POST)
  public Product active(@PathVariable String code) {
	    Product product = this.productService.findByCode(code);
      return this.productService.restore(product);
  }
  
}
