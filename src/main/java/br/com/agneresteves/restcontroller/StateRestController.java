package br.com.agneresteves.restcontroller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.agneresteves.domain.State;
import br.com.agneresteves.service.StateService;

@RestController
@RequestMapping("/api/states")
public class StateRestController {
  
    @Autowired
    private StateService stateService;
   
    @RequestMapping(method = RequestMethod.GET)
    public Iterable<State> findAll() {
    	return this.stateService.findAll();
    }
    
}
