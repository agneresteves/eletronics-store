package br.com.agneresteves.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import br.com.agneresteves.service.ProductService;

@Controller
@RequestMapping("/products")
public class ProductController {

	@Autowired
	private ProductService productService;
	
	private Map<String, Object> defaultParams() {
    	Map<String, Object> params = new HashMap<String, Object>();
    	return params;
    }
    
	@RequestMapping("")
	public ModelAndView home() {		
		Map<String, Object> params = this.defaultParams();

		params.put("products", this.productService.findAll());
		
		return new ModelAndView("pages/product/list", params);
	}
	
	@GetMapping("/new")
	public ModelAndView storeGet() {
		Map<String, Object> params = this.defaultParams();
		params.put("edit", false);
		return new ModelAndView("pages/product/form", params);
	}
	
	@GetMapping("/edit/{code}")
	public ModelAndView editGet() {
		Map<String, Object> params = this.defaultParams();
		params.put("edit", true);
		return new ModelAndView("pages/product/form", params);
	}
	
	@GetMapping("/{code}")
	public ModelAndView seeGet() {
		Map<String, Object> params = this.defaultParams();		
		return new ModelAndView("pages/product/see", params);
	}
	
}
