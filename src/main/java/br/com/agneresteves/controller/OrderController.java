package br.com.agneresteves.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/orders")
public class OrderController {

	private Map<String, Object> defaultParams() {
    	Map<String, Object> params = new HashMap<String, Object>();
    	return params;
    }
	
	@GetMapping("")
	public ModelAndView home() {
		return new ModelAndView("pages/order/list");
	}
	
	@GetMapping("/new")
	public ModelAndView storeGet() {
		Map<String, Object> params = this.defaultParams();
		params.put("edit", false);
		return new ModelAndView("pages/order/form", params);
	}
	
	@GetMapping("/{id}")
	public ModelAndView seeGet() {
		Map<String, Object> params = this.defaultParams();
		return new ModelAndView("pages/order/see", params);
	}
	
	@GetMapping("/edit/{id}")
	public ModelAndView editGet() {
		Map<String, Object> params = this.defaultParams();
		params.put("edit", true);
		return new ModelAndView("pages/order/form", params);
	}
	
}
