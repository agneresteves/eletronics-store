package br.com.agneresteves.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import br.com.agneresteves.domain.Client;
import br.com.agneresteves.service.ClientService;

@Controller
@RequestMapping("/clients")
public class ClientController {

	@Autowired
	private ClientService clientService;
		
    public ClientController() {
        
    }
	
    private Map<String, Object> defaultParams() {
    	Map<String, Object> params = new HashMap<String, Object>();
    	return params;
    }
    
	@RequestMapping("")
	public ModelAndView home() {		
		Map<String, Object> params = this.defaultParams();		
		return new ModelAndView("pages/client/list", params);
	}
	
	@GetMapping("/new")
	public ModelAndView storeGet() {
		Map<String, Object> params = this.defaultParams();
		params.put("edit", false);
		return new ModelAndView("pages/client/form", params);
	}

	@GetMapping("/{code}")
	public ModelAndView see(@PathVariable String code) {
		Map<String, Object> params = this.defaultParams();
	    
	  	Client client = this.clientService.findByCode(code);
	  
	  	if (client == null) {
		      return this.clientNotFound(params);
	    }
	  	
	  	params.put("client", client);
	  	return new ModelAndView("pages/client/see", params);
	}
	
	@GetMapping("/edit/{code}")
	public ModelAndView editGet(@PathVariable String code) {
	    Map<String, Object> params = this.defaultParams();
	    params.put("edit", true);
	    return new ModelAndView("pages/client/form", params);
	}
	
	private ModelAndView clientNotFound(Map<String, Object> params) {
		params.put("message", "Client inexistent!");
		params.put("status", "warning");
		return new ModelAndView("pages/client/result", params);
	}
}
