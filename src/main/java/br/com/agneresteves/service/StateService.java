package br.com.agneresteves.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.agneresteves.domain.State;
import br.com.agneresteves.repository.StateRepository;

@Service
public class StateService {

	@Autowired
	private StateRepository stateRepository;
	
	public Iterable<State> findAll() {
		return this.stateRepository.findAll();
	}
	
	public State findOne(int id) {
		return this.stateRepository.findOne(id);
	}
	
	public State createState(int code) {
		State state = new State();
		state.setId(code);
		return state;
	}
	
}
