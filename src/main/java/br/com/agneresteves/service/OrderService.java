package br.com.agneresteves.service;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.agneresteves.domain.Order;
import br.com.agneresteves.domain.OrderProduct;
import br.com.agneresteves.repository.OrderRepository;

@Service
public class OrderService {

	@Autowired
	private OrderRepository orderRepository;

	public Iterable<Order> findAll() {
		return this.orderRepository.findAll();
	}
	
	public Order save(Order order) {
		order.setCreatedAt(new Date());
		
		for (OrderProduct orderProduct: order.getOrderProducts()) {
			orderProduct.setOrder(order);
		}
		
		return this.orderRepository.save(order);
	}
	
	public Order cancel(Order order) {
		order.setDeletedAt(new Date());
		return this.update(order);
	}
	
	public Order restore(Order order) {
	    order.setDeletedAt(null);
	    return this.update(order);
	}
	
	public Order update(Order order) {
		Order orderOld = this.findOne(order.getId());
		
		if (orderOld.getStatus().getId() == 2) {
			return orderOld;
		}
		
	    order.setUpdatedAt(new Date());
	    
	    for (OrderProduct orderProduct: order.getOrderProducts()) {
			orderProduct.setOrder(order);
		}
	    
	    return this.orderRepository.save(order);
	}
	
	public Order findOne(long id) {
		return this.orderRepository.findOne(id);
	}
	
}
