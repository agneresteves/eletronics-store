package br.com.agneresteves.service;

import java.util.Date;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.agneresteves.domain.Client;
import br.com.agneresteves.domain.Contact;
import br.com.agneresteves.domain.State;
import br.com.agneresteves.repository.ClientRepository;

@Service
public class ClientService {

	@Autowired
	private ClientRepository clientRepository;
	
	@Autowired
	private StateService stateService;
	
	public Iterable<Client> findAll() {
		return this.clientRepository.findAll();
	}
	
	public Client save(Client client) {
		client.setCreatedAt(new Date());
		
		State state = this.stateService.findOne(client.getAddress().getState().getId());
		
		client.setCode(this.generateCode(state.getUf()));
		
		for (Contact contact: client.getContacts()) {
			contact.setClient(client);
		}
	  
		client.getAddress().setClient(client);
	  
		return this.clientRepository.save(client);
	}
	
	public Client update(Client client) {
	    client.setUpdatedAt(new Date());
	    
	    for (Contact contact: client.getContacts()) {
	    	contact.setClient(client);
	    }
  
	    client.getAddress().setClient(client);
	    
	    return this.clientRepository.save(client);
	}
	
	public Client findOne(String code) {
		return this.clientRepository.findByCode(code);
	}
	
	public Client inactive(Client client) {
		client.setDeletedAt(new Date());
		return this.update(client);
	}
	
	public Client active(Client client) {
	    client.setDeletedAt(null);
	    return this.update(client);
	}
	
	public String generateCode(String state) {
		Random rand = new Random();
		int  n = rand.nextInt(999999) + 1;
		
		return String.format("%s%06d", state, n);
	}
	
	public Client findByCode(String code) {
		return this.clientRepository.findByCode(code);
	}
	
}
