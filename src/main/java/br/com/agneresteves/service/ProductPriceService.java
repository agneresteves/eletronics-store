package br.com.agneresteves.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.agneresteves.domain.Product;
import br.com.agneresteves.domain.ProductPrice;
import br.com.agneresteves.repository.ProductPriceRepository;

@Service
public class ProductPriceService {

	@Autowired
	private RegionService regionService;
	
	@Autowired
	private ProductPriceRepository productPriceRepository;
	
	public List<ProductPrice> createProductPrice(Product product, List<Integer> regions, List<Double> prices) {
		List<ProductPrice> productsPrices = new ArrayList<ProductPrice>();
		
		for (int i = 0; i < regions.size(); i++) {
		    int code = regions.get(i);
		    double price = prices.get(i);
		    
		    ProductPrice pp = new ProductPrice();
	      pp.setRegion(this.regionService.createRegion(code));
	      pp.setPrice(price);
	      pp.setProduct(product);
	      productsPrices.add(pp);
		}
		
		return productsPrices;
	}
	
	public void store(ProductPrice productPrice) {
		this.productPriceRepository.save(productPrice);
	}
	
	public void store(List<ProductPrice> productsPrices) {
		for (ProductPrice pp: productsPrices) {
			this.store(pp);
		}
	}
	
}
