package br.com.agneresteves.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.agneresteves.domain.Region;
import br.com.agneresteves.repository.RegionRepository;

@Service
public class RegionService {

	@Autowired
	private RegionRepository regionRepository;
	
	public Iterable<Region> findAll() {
		return this.regionRepository.findAll();
	}
	
	public Region createRegion(int code) {
		Region region = new Region();
		region.setId(code);
		return region;
	}
	
}
