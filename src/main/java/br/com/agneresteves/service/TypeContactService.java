package br.com.agneresteves.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.agneresteves.domain.TypeContact;
import br.com.agneresteves.repository.TypeContactRepository;

@Service
public class TypeContactService {

	@Autowired
	private TypeContactRepository typeContactRepository;
	
	public Iterable<TypeContact> findAll() {
		return this.typeContactRepository.findAll();
	}
	
}
