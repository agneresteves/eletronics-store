package br.com.agneresteves.service;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.agneresteves.domain.Product;
import br.com.agneresteves.domain.ProductPrice;
import br.com.agneresteves.repository.ProductRepository;

@Service
public class ProductService {

	@Autowired
	private ProductRepository productRepository;
	
	public Product save(Product product) {
		product.setCreatedAt(new Date());
		
		for(ProductPrice pp: product.getProductPrices()) {
			pp.setProduct(product);
		}
		
		return this.productRepository.save(product);
	}
	
	public Iterable<Product> findAvailable() {
		return this.productRepository.findAvailable();
	}
	
	public Iterable<Product> findAll() {
		return this.productRepository.findAll();
	}
	
	public Product findByCode(String code) {
		return this.productRepository.findByCode(code);
	}
  
	public Product inactive(Product product) {
	    product.setDeletedAt(new Date());
	    return this.update(product);
	}
  
	public Product restore(Product product) {
		product.setDeletedAt(null);
		return this.update(product);
	}
  
	public Product update(Product product) {
		product.setUpdatedAt(new Date());    
		return this.productRepository.save(product);
	}
	
}
