package br.com.agneresteves.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.agneresteves.domain.Client;
import br.com.agneresteves.domain.Contact;
import br.com.agneresteves.repository.ContactRepository;

@Service
public class ContactService {
	
	@Autowired
	private ContactRepository contactRepository;
	
	public List<Contact> store(List<Contact> contacts, Client client) {
		List<Contact> list = new ArrayList<Contact>();
		
		for (Contact contact: contacts) {
			contact.setClient(client);
			list.add(this.contactRepository.save(contact));
		}
		
		return list;
	}
	
	public void deleteAllContacts(List<Contact> contacts) {
		this.contactRepository.delete(contacts);
	}
	
	public void deleteAllContactsClient(String code) {
		this.contactRepository.deleteByClientCode(code);
	}
	
	public List<Contact> findByCodeClient(String code) {
		return this.contactRepository.findByClientCode(code);
	}
	
}
