package br.com.agneresteves.domain;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "regions")
public class Region {

	@Id
	@GeneratedValue(strategy= GenerationType.IDENTITY)
	private int id;
	
	@Column
	@NotNull
	private String name;
	
	@JsonIgnore
	@OneToMany(mappedBy = "region", targetEntity = ProductPrice.class, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<ProductPrice> productsPrice;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	@Override
	public String toString() {
		return "["+this.id+" = "+this.name+"]";
	}

	public List<ProductPrice> getProductsPrice() {
		return productsPrice;
	}

	public void setProductsPrice(List<ProductPrice> productsPrice) {
		this.productsPrice = productsPrice;
	}

}
