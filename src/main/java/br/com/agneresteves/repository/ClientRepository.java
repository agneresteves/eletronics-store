package br.com.agneresteves.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.agneresteves.domain.Client;

@Repository
public interface ClientRepository extends CrudRepository<Client, String> {

	public Client findByCode(String code);
	
}
