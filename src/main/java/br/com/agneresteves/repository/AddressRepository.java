package br.com.agneresteves.repository;

import org.springframework.data.repository.CrudRepository;

import br.com.agneresteves.domain.Address;

public interface AddressRepository extends CrudRepository<Address, Integer> {

}
