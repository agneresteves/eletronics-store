package br.com.agneresteves.repository;

import org.springframework.data.repository.CrudRepository;

import br.com.agneresteves.domain.TypeContact;

public interface TypeContactRepository extends CrudRepository<TypeContact, Integer> {

}
