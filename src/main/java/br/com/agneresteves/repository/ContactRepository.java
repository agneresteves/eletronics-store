package br.com.agneresteves.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.agneresteves.domain.Contact;

@Repository
public interface ContactRepository extends CrudRepository<Contact, Integer> {
	
    @Modifying
    @Transactional
    @Query("delete from Contact c where c.client.code = :code")
    public void deleteByClientCode(@Param("code") String code);
	
    public List<Contact> findByClientCode(String clientCode);
	
}