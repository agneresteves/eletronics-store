package br.com.agneresteves.repository;

import org.springframework.data.repository.CrudRepository;

import br.com.agneresteves.domain.State;

public interface StateRepository extends CrudRepository<State, Integer> {

}
