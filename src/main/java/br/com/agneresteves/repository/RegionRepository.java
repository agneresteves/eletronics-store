package br.com.agneresteves.repository;

import org.springframework.data.repository.CrudRepository;

import br.com.agneresteves.domain.Region;

public interface RegionRepository extends CrudRepository<Region, Integer> {

}
