package br.com.agneresteves.repository;

import org.springframework.data.repository.CrudRepository;

import br.com.agneresteves.domain.ProductPrice;

public interface ProductPriceRepository extends CrudRepository<ProductPrice, Integer> {

}
