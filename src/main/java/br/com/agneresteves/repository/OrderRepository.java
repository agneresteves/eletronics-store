package br.com.agneresteves.repository;

import javax.transaction.Transactional;

import org.springframework.data.repository.CrudRepository;

import br.com.agneresteves.domain.Order;

@Transactional
public interface OrderRepository extends CrudRepository<Order, Long> {

}
