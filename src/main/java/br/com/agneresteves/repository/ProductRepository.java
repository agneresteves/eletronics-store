package br.com.agneresteves.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import br.com.agneresteves.domain.Product;

public interface ProductRepository extends CrudRepository<Product, Integer> {
	
	@Query(value = "select p.* from products as p where p.id in (select max(p2.id) from products as p2 group by (p2.code))",
			nativeQuery = true)
	public Iterable<Product> findAll();
	
	@Query(value = "select p.* from products as p where p.id in (select max(p2.id) from products as p2 group by (p2.code)) and deleted_at is null",
			nativeQuery = true)
	public Iterable<Product> findAvailable();
	
	@Query(value = "select * from products p where code = ?1 order by id desc limit 1",
			nativeQuery = true)
	public Product findByCode(String code);
  
}
